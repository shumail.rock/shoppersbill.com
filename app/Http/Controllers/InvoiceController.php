<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Item;
use App\Purchaser;
use App\Seller;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['create']);
    }

    public function index()
    {
        $invoiceData=Invoice::where('user_id',auth()->user()->id)->orderBy('id','DESC')->paginate(10);
        //dd($invoiceData);
        return view('invoice.index',compact('invoiceData'));

    }

    public function create()
    {
        return view('invoice.create');
    }

    public function store(Request $request)
    {
        if(isset($_POST['img_data'])){
        $imagedata = base64_decode($_POST['img_data']);
	    $signature = md5(date("dmYhisA"));
	    $sig_name = 'inovieLogos/'.$signature.'.png';
        file_put_contents($sig_name,$imagedata);
         $sig_name=$signature.'.png';         
        }else{
         $sig_name=''; 
        }
        $logo = isset($request['logo']) ? $request['logo']:0;
        if($logo){
            $orignalName= $logo->getClientOriginalName();
            $name= time().rand('00000000','99999999').$orignalName;
            $logo->move('inovieLogos',$name);
            $logoFile = $name;
        }else{
            $logoFile='';
        }

        $purchaser=Purchaser::create([
            'pur_name'=>$request->purName,
            'pur_address'=>$request->purAddress,
            'pur_tax_id'=>$request->purTaxId,
            'pur_date'=>$request->purDate,
            'pur_status'=>'1',
        ]);


        $seller=Seller::create([
            'seller_name'=>$request->sellerName,
            'seller_phone'=>$request->sellerPhn,
            'seller_address'=>$request->sellerAddress,
            'user_id'=>auth()->user()->id,
            'seller_tax_id'=>$request->sellerTaxId,
            'seller_co_information'=>$request->sellerCoInfo,
            'seller_status'=>'1',
        ]);

        if($seller!==null && $purchaser!==null){

            $invoice=Invoice::create([
                'user_id'=>auth()->user()->id,
                'seller_id'=>$seller->id,
                'inv_number'=>$request->invoiceNumber,
                'inv_currency'=>$request->currency,
                'inv_type'=>$request->invoiceType,
                'inv_dis_per'=>$request->disRate,
                'inv_tax_type'=>$request->taxType,
                'inv_tax_per'=>$request->taxRate,
                'purchaser_id'=>$purchaser->id,
                'inv_logo'=>$logoFile,
                'inv_signature'=>$sig_name,
                'inv_status'=>'1',

            ]);
           if($invoice!==null){


                  for($i=0;$i<count($request->itemName);$i++){

                      Item::create([
                          'inv_item_name'=>$request->itemName[$i],
                          'inv_item_price'=>$request->itemPrice[$i],
                          'inv_item_quantity'=>$request->itemQty[$i],
                          'invoice_id'=>$invoice->id,
                          'inv_item_status'=>'1',
                      ]);

                  }



               return redirect()->back()->with('success','Invoice Created Successfully, You can see it your Account Dashboard!');
           }else{
               return redirect()->back()->with('error','Something Went Wrong Please Try Again!');
           }
        }else{
            return redirect()->back()->with('error','Something Went Wrong Please Try Again!');
        }
    }


    public function show($id)
    {
        $invoice=Invoice::where('id',$id)->get()->first();
        return view('invoice.showinvoice',compact('invoice'));
    }


    public function edit($id)
    {
        $invoiceData=Invoice::where('id',$id)->get()->first();
        return view('invoice.edit',compact('invoiceData'));
    }

    public function update(Request $request, $id)
    {

        if(isset($_POST['img_data'])){
        $imagedata = base64_decode($_POST['img_data']);
	    $signature = md5(date("dmYhisA"));
	    $sig_name = 'inovieLogos/'.$signature.'.png';
        file_put_contents($sig_name,$imagedata);
         $sig_name=$signature.'.png';         
        }else{
         $sig_name=''; 
        }
        $invoicereq=Invoice::where('id',$id)->get()->first();

        $purchaser=Purchaser::findorfail($invoicereq->purchaser_id)->update([
            'pur_name'=>$request->purName,
            'pur_address'=>$request->purAddress,
            'pur_tax_id'=>$request->purTaxId,
            'pur_date'=>$request->purDate,
            'pur_status'=>'1',
        ]);


        $seller=Seller::findorfail($invoicereq->seller_id)->update([
            'seller_name'=>$request->sellerName,
            'seller_phone'=>$request->sellerPhn,
            'seller_address'=>$request->sellerAddress,
            'user_id'=>auth()->user()->id,
            'seller_tax_id'=>$request->sellerTaxId,
            'seller_co_information'=>$request->sellerCoInfo,
            'seller_status'=>'1',
        ]);

        if($seller!==null && $purchaser!==null){
            $logo = isset($request['logo']) ? $request['logo']:0;
            if($logo){
                $orignalName= $logo->getClientOriginalName();
                $name= time().rand('00000000','99999999').$orignalName;
                $logo->move('inovieLogos',$name);
                $logoFile = $name;

                $invoice=Invoice::findorfail($invoicereq->id)->update([
                    'user_id'=>auth()->user()->id,
                    'seller_id'=>$invoicereq->seller_id,
                    'inv_number'=>$request->invoiceNumber,
                    'inv_currency'=>$request->currency,
                    'inv_type'=>$request->invoiceType,
                    'inv_dis_per'=>$request->disRate,
                    'inv_tax_type'=>$request->taxType,
                    'inv_tax_per'=>$request->taxRate,
                    'purchaser_id'=>$invoicereq->purchaser_id,
                    'inv_logo'=>$logoFile,
                    'inv_signature'=>$sig_name,
                    'inv_status'=>'1',

                ]);
            }
            else{
                $invoice=Invoice::findorfail($invoicereq->id)->update([
                    'user_id'=>auth()->user()->id,
                    'seller_id'=>$invoicereq->seller_id,
                    'inv_number'=>$request->invoiceNumber,
                    'inv_currency'=>$request->currency,
                    'inv_type'=>$request->invoiceType,
                    'inv_dis_per'=>$request->disRate,
                    'inv_tax_type'=>$request->taxType,
                    'inv_tax_per'=>$request->taxRate,
                    'purchaser_id'=>$invoicereq->purchaser_id,
                    'inv_signature'=>$sig_name,
                    'inv_status'=>'1',

                ]);
            }


            if($invoice!==null){
                for($i=0;$i<count($request->itemName);$i++){
                    Item::where('id',$request->itemId[$i])->update([
                        'inv_item_name'=>$request->itemName[$i],
                        'inv_item_price'=>$request->itemPrice[$i],
                        'inv_item_quantity'=>$request->itemQty[$i],
                        'invoice_id'=>$invoicereq->id,
                        'inv_item_status'=>'1',
                    ]);

                }


                return redirect('invoice')->with('success','Invoice Created Successfully, You can see it your Account Dashboard!');
            }else{
                return redirect()->back()->with('error','Something Went Wrong Please Try Again!');
            }
        }else{
            return redirect()->back()->with('error','Something Went Wrong Please Try Again!');
        }
    }

    public function destroy($id)
    {
        //
    }

    public function GeneratePdf($id){
        $invoice=Invoice::where('id',$id)->get()->first();
//        return view('invoice.invoicepdf',compact('invoice'));
        $pdf = \PDF::loadView('invoice.invoicepdf',compact('invoice'));
        return $pdf->download('invoice'.$invoice->inv_number.'.pdf');
    }

    public function InvoiceSearch(Request $request)
    {
        $q = Input::get ('search');
        $user_id=auth()->user()->id;
        $invoice = Invoice::where ( 'id', '=', $q )->orWhere ( 'inv_number', '=',$q)->where('user_id',$user_id)->get();
        if (count ( $invoice ) > 0)
            return view ( 'invoice.search',compact('invoice','q') );
        else
            return view ( 'invoice.search',compact('invoice','q') );
    }
//
//        if ($request->ajax()) {
//            $output = "";
//            $invoice = DB::table('Invoice')->where('id', 'LIKE', '%' . $request->search . "%")->get();
//            if ($invoice) {
//                foreach ($invoice as $key => $inv) {
//                    $output .= '<tr>' .
//                        '<td>' . $inv->id . '</td>' .
//                        '<td>' . $inv->inv_number . '</td>' .
//                        '<td>' . $inv->inv_type . '</td>' .
//                        '<td>' . $inv->inv_currency . '</td>' .
//                        '</tr>';
//                }
//                return Response($output);
//            }
//        }
//    }

}
