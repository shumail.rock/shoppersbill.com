<?php

namespace App\Http\Controllers;

use App\User;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('signup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'email'=>['required','email','unique:users'],
            'firstname'=>['required','string','regex:/^[a-zA-Z]+$/u'],
            'lastname'=>['required','string','regex:/^[a-zA-Z]+$/u'],
            'password'=>['required','string','confirmed'],
            'membershipType'=>['required']
       ]);
       if($request->membershipType==='1'){
         $amount=400;
       }else if($request->membershipType=='2'){
         $amount=600;
       }else if($request->membershipType=='3'){
         $amount=1000;
       }

        $curl = curl_init();

        $email = $request->email;
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode([
                'amount'=>$amount,
                'email'=>$email
            ]),
            CURLOPT_HTTPHEADER => [
                "authorization: Bearer sk_test_cb1386228f68770ff2968cc24458304030c7b81a", //replace this with your own test key
                "content-type: application/json",
                "cache-control: no-cache"
            ],
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if($err){
            // there was an error contacting the Paystack API
            die('Curl returned error: ' . $err);
        }

        $tranx = json_decode($response, true);

        if($tranx['status']){
            Session::put('userData', $request->all());
            $payment_url=$tranx['data']['authorization_url'];
            header('Location: ' . $tranx['data']['authorization_url']);
        }else{
            return redirect()->back()->with('error','Oops Something Went Wrong Please try Again!');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function paystackCallBack(Request $request){
        $trxref=$request->trxref;
        $reference=$request->reference;
        /* Verify payment */
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . rawurlencode($reference),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "authorization: Bearer sk_test_cb1386228f68770ff2968cc24458304030c7b81a",
                "cache-control: no-cache"
            ],
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if($err){
            return redirect()->route('signup')->with('error','Oops Something Went Wrong Please try Again!');
        }

        $tranx = json_decode($response);

        if($tranx->data->status=='success'){
            $userData=Session::get('userData');
            $result=User::create([
                'email'=>$userData['email'],
                'name'=>$userData['firstname'],
                'lname'=>$userData['lastname'],
                'password'=>Hash::make($userData['password']),
            ]);

            $payment_start_date=date('Y-m-d H:i:s');
            if($userData['membershipType']=='1'){
                $payment_end_date=date('Y-m-d H:i:s',strtotime('+ 7 Day',strtotime(date('Y-m-d H:i:s'))));
            }else if($userData['membershipType']=='2'){
                $payment_end_date=date('Y-m-d H:i:s',strtotime('+ 1 Month',strtotime(date('Y-m-d H:i:s'))));
            }else if($userData['membershipType']=='3'){
                $payment_end_date=date('Y-m-d H:i:s',strtotime('+ 12 Month',strtotime(date('Y-m-d H:i:s'))));
            }
            if($result!==null) {
                $result = Payment::create([
                    'user_id' => $result->id,
                    'payment_start_date' => $payment_start_date,
                    'payment_end_date' => $payment_end_date,
                    'payment_status' => $tranx->data->status,
                    'payment_amount' => $tranx->data->amount,
                    'payment_currency' => $tranx->data->currency,
                    'payment_ip' => $tranx->data->ip_address,
                    'payment_channel' => $tranx->data->channel,
                    'membership_type' => $userData['membershipType']

                ]);
                if ($result !== null) {
                return redirect()->route('signup')->with('success', 'You Have Been Registered Successfully!');
                }else{
                    return redirect()->route('signup')->with('error','Oops Something Went Wrong Please try Again!');
                }
            }else{
                return redirect()->route('signup')->with('error','Oops Something Went Wrong Please try Again!');
            }


        }else{
            return redirect()->route('signup')->with('error','Oops Something Went Wrong Please try Again!');
        }

        /*end verification */


    }
}
