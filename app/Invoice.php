<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
   protected $fillable=['user_id','inv_number','seller_id','inv_currency','inv_type','inv_tax_per','inv_dis_per','inv_tax_type','inv_status','purchaser_id','inv_logo','inv_signature'];

   public  function user(){

       return $this->belongsTo(User::class);

   }

    public  function item(){
       return $this->hasMany(Item::class);
    }
    public function seller(){
       return $this->belongsTo(Seller::class);
    }
    public function purchaser(){
       return $this->belongsTo(Purchaser::class);
    }
}
