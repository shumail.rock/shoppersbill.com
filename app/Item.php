<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable=['inv_item_name','inv_item_price','inv_item_quantity','invoice_id','inv_item_status'];

    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }
}
