<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable=['user_id','payment_start_date','payment_end_date','payment_status','payment_amount','payment_currency','payment_ip','payment_channel','membership_type'];

    public function user(){
        return $this->hasOne(User::class);
    }
}
