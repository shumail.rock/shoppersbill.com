<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchaser extends Model
{
    protected $fillable=['pur_name','pur_address','pur_tax_id','pur_status','pur_date'];

    public function invoice(){
        return $this->hasMany(Invoice::class);
    }
}
