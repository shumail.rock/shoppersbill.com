<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable=['seller_name','seller_phone','seller_address','user_id','seller_tax_id','seller_co_information','seller_status'];

    public function invoice(){
        return $this->hasMany(Invoice::class);
    }
}
