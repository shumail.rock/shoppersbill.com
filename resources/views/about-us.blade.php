@extends('layouts.master')
@section('headerbox')
    <div class="welcome-text-area white" >
        <div class="area-bg"></div>
        <div class="welcome-area">
            <div class="container">
                <div class="row flex-v-center">
                    <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                        <div class="welcome-mockup center">
                            <img src="img/home/watch-mockup.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                        <div class="welcome-text">
                            <h1>Welcome To ShoppersBill Invoice Apps.</h1>
                            <p> Generate Invoice and Reciept for everyone Including - shop Owners, Business, Freelancers,, store owners,Ecommerce, Transactions, Deals e.t.c.

                                .</p>
                            <div class="home-button">
                                <a href="#">Create Receipt / Invoice</a>
                                <a href="#">Search for Invoice </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
<!--FEATURES TOP AREA-->
<section class="features-top-area padding-100-50" id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>About<span>ShoppersBill</span></h2>
                        <span class="icon-and-border"><i class="material-icons">phone_android</i></span>
                        <h3> <p>ShoppersBill is an Online Invoicing and Reciept generation Platform . For shop Owners, Business, Freelancers, and store owners,.</p>

<p>Enabling Individulas Generate and save invoice created for Customers or partners for Transactionon done, either in person or online .</p>

<p>Allowing individuals keep a record of their Invoice on our Platform that can be searched for anywhere around the world .</p>

<p>Search for Invoice, Receipt, fliers, Certficates, documents and Paper-works generated on our Platfrom.</p>

<p> Upload your Documents Invoice Certificates and Reciept ,let us help you save and keep it, Your Documents can been seen from all over the world. </p>

<p> Get to Create and save Beautifully designed Invoice and Receipt that can be generated and viewed from anywhere in the world </p> </h3>

<p>-</p>


<p> <h2> The Security and Speed of e-Invoicing </h2> </p>
<h3><p>ShoppersBill gives businesses solutions to security problems that paper processes are the cause of. Paper-based processes bring the fear of catching manual mistakes when its too late. When paired with data encryption and firewalls, Shoppersbill provides the security for sensitive vendor and payment information that a file cabinet cannot 100% guarantee. With invoicing management software, organizations have control over the security of their data because the system includes many internal safety features such as validation requirements, duplicate scans, and various degrees of approval requirements based on your companys current processes.</p> </h3>

<p>-</p>

<p> <h2> Amongst the Issues ShoppersBill is Trying to solve are </h2> </p>
<h3> <p>*	Loss of Paper Receipt and Invoice</p>
<p>	* Loss of Generated Bill</p>
<p>	* Stolen Receipt</p>
<p>	* Creation and entry errors on both sides</p>
<p>	* High operational costs per invoice on both sender and receiver side</p>
<p>	* Even more costs in the case of errors or disputes</p>
<p>	* Involvement of multiple systems</p>
<p>	* Database for all Invoice Receipt and Bills</p>
<p>	* View Invoices and Receipts after Purchase</p> </h3>   

<p> - </p>
<p> - </p>  

<h3> <p>For the last several years, business partners have relied more frequently on electronic invoicing, also known as e-invoicing, through services like our company - ShoppersBill. This is simply the electronic transfer of billing and payment information between the supplier and buyer.</p> </h3>

<p> <h2>For buyers, the benefits of ShoppesrBill include:</h2> </p>
<h3><p>	* Fast Receipt and Invoice</p>
<p>	* Steady Record of Invoice and Receipt</p>
<p>	* Large Database Record</p>
<p>	* Reduction in costs</p>
<p>	* Faster processing and payment cycle</p>
<p>	* Improved cash flow management</p>
<p>	* Increase in accuracy</p>
<p>	* Reduction in the use of Paper, Receipt, Invoice & Bill </p>	
<p>	* Reduction in late payment fees, fraud, and duplicate invoices</p>


<p> <h2>For suppliers, the benefits of ShoppersbIll include:</h2> </p>
<h3><p>	* Faster payments</p>
<p>	* Increase in productivity</p>
<p>	* Fewer rejected invoices</p>
<p>	* Improved cash flow management</p>
<p>	* Additional finance options</p>

<p> - </p>
<p> - </p>

<h3><p>As for managers, Shoppersbill will assist with any green initiatives that your organization has implemented, as well improving visibility and strengthening supplier/customer relationships.</p></h3>


<p> <h2>Types of Service we provide Includes </h2> </p>
<h3><p>There are several different types of services we are offering to the general Public, The most common invoices used are:</p>
<p>	* Invoice Generation (Invoice as a service)</p>
<p>	* Receipt and Bill Generation</p>
<p>	* Proposal/Bid/Pro Forma. An expectation of how much a business is going to charge for goods or services. As opposed to a bill, this is a preliminary appraisal.</p>
<p>	* Interim Invoice. Invoices that are sent out weekly or monthly during a long-term project.</p>
<p>	* Recurring Invoices. Used for recurring customers. Shoppersbill can automate these invoices.</p>
<p>	* Past Due Invoices. Sent when an account is past due. Includes original invoice and interest accrued.</p>
<p>	* Final Invoice. An invoice that is sent out following the completion of a project.</p>
<p>	* Certificate Generation (Marriage, law. School e.t.c)</p>
<p>	Forms and Grid Forms Generation</p></h3>


<p> <h2> Contact us</h2>. </p>
<p> <h3>Please Call us @ - <h3 ></p>,    <p> <h3>(+234) 08034071475, o7086374876, 08160669808, </h3> </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="qs-box relative mb50 center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="qs-box-icon">
                      <img src="img/icon/icon-1.png" height="70" width="70" alt="happy" class="picture">
                        </div>
                        <h3>Upload & Save</h3>
                        <p>Upload your Documents Invoice Certificates and Reciept ,let us help you save and keep it, Your Documents can been seen from all over the world .</p>
                        <a href="/create/" class="read-more"> Click to Upload</a>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="qs-box relative mb50 center  wow fadeInUp" data-wow-delay="0.3s">
                        <div class="qs-box-icon">
                   <img src="img/icon/11.png" height="90" width="80" alt="happy" class="picture">
                        </div>
                        <h3>Create Invoice</h3>
                        <p>Get to Create and save Beautifully designed Invoice and Receipt that can be generated and viewed from anywhere in the world.</p>
                        <a href="/create/" class="read-more">Click to Create</a>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="qs-box relative mb50 center  wow fadeInUp" data-wow-delay="0.4s">
                        <div class="qs-box-icon">
                            <img src="img/icon/icon3.png" height="90" width="80" alt="happy" class="picture">                        </div>
                        <h3>Search for Invoice</h3>
                        <p>Search for Invoice, Receipt, fliers,  Certficates, documents and Papew-works generated on our Platfrom.</p>
                        <a href="https://shoppersbill.com/logines/invoices.php" class="read-more">Click to Search</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FEATURES TOP AREA END-->
@endsection