@extends('layouts.master')
@section('content')
<div class="row" style="margin:0px;padding: 5% 0px;background-image: linear-gradient(to bottom, #1193d4 ,#1193d4,#1193d4,transparent,transparent); ">

</div>
<div class="container">
    <div class="">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('error')}}
            </div>
        @endif
        @if($errors->has('email'))
            <p class="error">{{$errors->first('email')}}</p>
        @endif
        <form class="form-horizontal" action="{{route('invoice.store')}}" method="post" role="form" enctype="multipart/form-data" id="form">
            @csrf
            <legend>Seller Information</legend>
            <span class="btn btn-small btn-info change-seller-info">Edit Seller Info</span>

            <div class="company-info hide">
                <div class="form-group">
                    <label for="company-name" class="col-sm-2 control-label">Company name</label>

                    <div class="col-xs-4">
                        <input type="text" name="sellerName" class="form-control" id="company-name" placeholder="My company Inc." required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="company-address" class="col-sm-2 control-label">Address</label>

                    <div class="col-xs-4">
                        <input name="sellerAddress" type="text" class="form-control" id="company-address" placeholder="No 10 company street,My company Address"
                               required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="company-tin" class="col-sm-2 control-label">TAX ID No.(optional)</label>

                    <div class="col-xs-4">
                        <input name="sellerTaxId" type="text" class="form-control" id="company-tin" placeholder="07086947245" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="billNo" class="col-sm-2 control-label">Invoice Number (optional)</label>

                    <div class="col-xs-4">
                        <input name="invoiceNumber" type="text" class="form-control" id="billNo" placeholder="23341" required>
                    </div>
                </div>


                <div class="form-group">
                    <label for="company-contact" class="col-sm-2 control-label">Phone number</label>

                    <div class="col-xs-4">
                        <input type="text" name="sellerPhn" class="form-control" id="company-contact" placeholder="+91 9945 153216"
                               required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="company-contact" class="col-sm-2 control-label">Co-seller Info.(optional)</label>

                    <div class="col-xs-4">
                        <input type="text" name="sellerCoInfo" class="form-control" id="company-contact" placeholder="Co- seller Name & Number"
                               required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="company-currency" class="col-sm-2 control-label">Currency</label>

                    <div class="col-sm-4">
                        <select class="form-control" id="company-currency" name="currency">
                            <option value="N">NGN</option>
                            <option value="GBP">GBP</option>
                            <option value="$">USD</option>
                            <option value="$">CAD</option>
                            <option value="Y">YEN</option>
                            <option value="Z">ZAR</option>
                        </select>
                    </div>
                </div>
            </div>

            <legend>Configuration</legend>
            <div class="form-group">
                <label for="invoice-type-radio" class="col-sm-2 control-label">Invoice Type</label>

                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="radio" name="invoiceType" value="Tax"> Tax invoice
                        </label>
                        <label>
                            <input type="radio" id="invoice-type-radio" name="invoiceType" value="Retail" checked>
                            Retail invoice
                        </label>
                    </div>
                </div>
            </div>

            <legend>Purchaser Info.</legend>
            <div class="form-group">
                <label for="billNo" class="col-sm-2 control-label">Date</label>

                <div class="col-sm-4">
                    <input type="date" class="form-control" id="billDate" name="purDate" placeholder="Select Date" required>
                </div>
            </div>

            <div class="form-group">
                <label for="purchaser" class="col-sm-2 control-label" >Name</label>

                <div class="col-xs-4">
                    <input type="text" class="form-control" id="purchaser" placeholder="Mr John Doe" required name="purName">
                </div>
            </div>

            <div class="form-group">
                <label for="purchaser-address" class="col-sm-2 control-label">Address</label>

                <div class="col-xs-4">
                    <textarea name="purAddress" rows="3" class="form-control" id="purchaser-address"
                              placeholder="Purchaser's address"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="purchaser-tin" class="col-sm-2 control-label">Tax Id No.</label>

                <div class="col-xs-4">
                    <input type="text" class="form-control" name="purTaxId" id="purchaser-tin" placeholder="Purchaser's TIN/PAN">
                </div>
            </div>

            <legend>Items</legend>
            <div class="row heading">
                <div class="col-sm-1"></div>
                <div class="col-sm-5">-Name</div>
                <div class="col-sm-2">-Price</div>
                <div class="col-sm-2">-Unit/Quantity</div>
            </div>

            <div class="items">
                <div class="row item" data-serial="1">
                    <div class="col-sm-1 sr-no">1</div>
                    <div class="col-sm-5">
                        <input class="form-control item-name" name="itemName[]" type="text">
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon currency">N</span>
                            <input class="form-control item-price" name="itemPrice[]" type="number">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control item-quantity" name="itemQty[]" value="1" type="number">
                    </div>
                    <div class="col-sm-2">
                        <p class="form-control-static item-row-price"><span class="currency">N</span> <span
                                    class="numeral">0</span></p>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-1 col-sm-2">
                    <span class="btn btn-primary add-item">Add item</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Total</label>

                <div class="col-sm-offset-8 col-sm-2">
                    <p class="form-control-static item-total"><span class="currency">N</span> <span
                                class="numeral">0</span></p>
                </div>
            </div>

            <legend>Discount</legend>
            <div class="form-group">
                <label for="discount" class="col-sm-2 control-label">Percentage</label>

                <div class="col-sm-2">
                    <div class="input-group">
                        <input type="number" class="form-control" name="disRate" id="discount" value="0" placeholder="Discount"
                               max="100" min="0">
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
                <div class="col-sm-offset-6 col-sm-2">
                    <p class="form-control-static discount-amount">- <span class="currency">N</span> <span
                                class="numeral">0</span></p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Total</label>

                <div class="col-sm-offset-8 col-sm-2">
                    <p class="form-control-static discount-total"><span class="currency">N</span> <span
                                class="numeral">0</span></p>
                </div>
            </div>

            <legend>Taxes</legend>
            <div class="form-group">
                <label for="tax-type-check" class="col-sm-2 control-label">Tax Type</label>

                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="radio" name="taxType" value="VAT"> VAT
                        </label>
                        <label>
                            <input type="radio" id="tax-type-check" name="taxType" value="CST" checked> CST
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="vat" class="col-sm-2 control-label">Percentage</label>

                <div class="col-sm-2">
                    <div class="input-group">
                        <input type="number" class="form-control" id="vat" name="taxRate" value="12.5" placeholder="VAT (12.5%)"
                               step="0.1">
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
                <div class="col-sm-offset-6 col-sm-2">
                    <p class="form-control-static vat-amount"><span class="currency">N</span> <span
                                class="numeral">0</span></p>
                </div>
            </div>

            <div class="service-tax-group hide">
                <div class="form-group">
                    <label for="service-tax" class="col-sm-2 control-label">Service Tax</label>

                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="number" class="form-control" id="service-tax" value="0"
                                   placeholder="Service Tax" step="0.1">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="col-sm-offset-6 col-sm-2">
                        <p class="form-control-static service-tax-amount"><span class="currency">N</span> <span
                                    class="numeral">0</span></p>
                    </div>
                </div>

                <div class="service-tax-extras">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Swachchh Bharat Cess (0.5%)</label>

                        <div class="col-sm-offset-6 col-sm-2">
                            <p class="form-control-static bharat-cess"><span class="currency">N</span> <span
                                        class="numeral">0</span></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Krishi Kalyan Cess (0.5%)</label>

                        <div class="col-sm-offset-6 col-sm-2">
                            <p class="form-control-static kk-cess"><span class="currency">N</span>
                                <span class="numeral">0</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            @if(auth()->check())
            <div class="form-group">
                <label class="col-sm-2 control-label">Invoice Logo</label>

                <div class="col-sm-offset-8 col-sm-2">
                    <input type="file" name="logo" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Signature</label>

                <div class="col-sm-offset-6 col-sm-4">
        		<div id="signArea" >
        			<div class="sig sigWrapper" style="height:auto;">
        				<div class="typed"></div>
        			    	<canvas class="sign-pad" id="sign-pad" width="350" height="100"></canvas>
                         <input type="hidden" value="" id="imgurl" name="img_data" />
                    </div>
        		</div>
                </div>
            </div>            

            @endif
            <!--<div class="form-group">-->
            <!--<label class="col-sm-2 control-label">Total</label>-->
            <!--<div class="col-sm-offset-8 col-sm-2">-->
            <!--<p class="form-control-static total-with-tax">&#8377; 0</p>-->
            <!--</div>-->
            <!--</div>-->

            <hr/>
            <!--<div class="form-group">-->
            <!--<label class="col-sm-2 control-label">Round off</label>-->
            <!--<div class="col-sm-offset-8 col-sm-2">-->
            <!--<p class="form-control-static round-off">+ &#8377; 0.00</p>-->
            <!--</div>-->
            <!--</div>-->

            <div class="form-group">
                <label class="col-sm-2 control-label">Total payable</label>

                <div class="col-sm-offset-8 col-sm-2">
                    <p class="form-control-static total-with-tax"><span class="currency">N;</span> <span
                                class="numeral">0</span></p>
                </div>
            </div>
            <hr/>

        <div class="row">
            <div class="col-sm-10">
                <div class="btn-group-lg">
                    <button class="btn btn-success generate-bill">Generate & Download Receipt</button>
                    <button class="btn disabled hide generate-blank-bill" title="Blank bill is currently disabled">
                        Generate Blank Bill
                    </button>
                    <button class="btn btn-primary new-bill">Start New</button>
                    @if(auth()->check())
                    <button type="submit" class="btn btn-warning" role="button" id="btnSaveSign">Save Invoice Online</button>
                    @else
                    <a href="{{route('pricing')}}" target="_blank" class="btn btn-warning" role="button">Save Invoice Online</a>
                    @endif

                   <!-- <a href="https://shoppersbill.com/logines/invoices.php" class="btn btn-mybutton" role="button">Search</a>
                    <a href="https://shoppersbill.com/logins/login.php" class="btn btn-mybutton" role="button">Login</a>
                    <a href="https://shoppersbill.com/invos" class="btn btn-mybutton" role="button">Send Invoice</a>-->
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<div class="row item-template hide" data-serial="0">
    <div class="col-sm-1 sr-no">0</div>
    <div class="col-sm-5">
        <input class="form-control item-name" name="itemName[]" type="text">
    </div>
    <div class="col-sm-2">
        <div class="input-group">
            <span class="input-group-addon currency">N</span>
            <input class="form-control item-price" name="itemPrice[]" type="number">
        </div>
    </div>
    <div class="col-sm-2">
        <input class="form-control item-quantity" name="itemQty[]" value="1" type="number">
    </div>
    <div class="col-sm-2">
        <p class="form-control-static item-row-price"><span class="currency">N</span> <span
                    class="numeral">0</span></p>
    </div>
</div>
<div class="row" style="padding: 5% 0px;margin:0px;">

</div>
@endsection
@section('script')
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="{!! asset('js/numeric-1.2.6.min.js') !!}"></script> 
<script src="{!! asset('js/bezier.js') !!}"></script>
<script src="{!! asset('js/jquery.signaturepad.js') !!}"></script> 
	
<script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
<script src="{!! asset('js/json2.min.js') !!}"></script>  
<script>
			$(document).ready(function() {
				$('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
			}); 
			$("#form").on('submit',function(e){
			    e.preventDefault();
				html2canvas([document.getElementById('sign-pad')], {
					onrendered: function (canvas) {
						var canvas_img_data = canvas.toDataURL('image/png');
						var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
						$('#imgurl').val(img_data);
                        $('#form').unbind('submit').submit();
					}
				});
			});			
</script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="{{asset('js/pdfmake.js')}}"></script>
<script src="{{asset('js/vfs_fonts.js')}}"></script>
<script src="{{asset('js/enums.js')}}"></script>
<script src="{{asset('js/utils.js')}}"></script>
<script src="{{asset('js/text_number.js')}}"></script>
<script src="{{asset('js/bill_writer.js')}}"></script>
<script src="{{asset('js/invoiceMain.js')}}"></script>
@endsection