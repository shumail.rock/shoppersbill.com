@extends('layouts.master')
@section('content')

    {{--@dd($invoiceData);--}}
    <div class="row" style="margin:0px;padding: 5% 0px;background-image: linear-gradient(to bottom, #1193d4 ,#1193d4,#1193d4,transparent,transparent); ">

    </div>
    <div class="container">
        <div class="">
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('error')}}
                </div>
            @endif
            @if($errors->has('email'))
                <p class="error">{{$errors->first('email')}}</p>
            @endif
            <form class="form-horizontal" action="{{route('invoice.update',$invoiceData->id)}}"  method="post" role="form" enctype="multipart/form-data" id="form">
                @csrf
                @method("PATCH")
                <legend>Seller Information</legend>

                <div class="company-info">
                    <div class="form-group">
                        <label for="company-name" class="col-sm-2 control-label">Company name</label>

                        <div class="col-xs-4">
                            <input type="text" name="sellerName" class="form-control" id="company-name" value="{{$invoiceData->seller->seller_name}}"required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company-address" class="col-sm-2 control-label">Address</label>

                        <div class="col-xs-4">
                            <input name="sellerAddress" type="text" class="form-control" id="company-address" value="{{$invoiceData->seller->seller_address}}"required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company-tin" class="col-sm-2 control-label">TAX ID No.(optional)</label>

                        <div class="col-xs-4">
                            <input name="sellerTaxId" type="text" class="form-control" id="company-tin" value="{{$invoiceData->seller->seller_tax_id}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="billNo" class="col-sm-2 control-label">Invoice Number (optional)</label>

                        <div class="col-xs-4">
                            <input name="invoiceNumber" type="text" class="form-control" id="billNo" value="{{$invoiceData->inv_number}}" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="company-contact" class="col-sm-2 control-label">Phone number</label>

                        <div class="col-xs-4">
                            <input type="text" name="sellerPhn" class="form-control" id="company-contact" value="{{$invoiceData->seller->seller_phone}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company-contact" class="col-sm-2 control-label">Co-seller Info.(optional)</label>

                        <div class="col-xs-4">
                            <input type="text" name="sellerCoInfo" class="form-control" id="company-contact" value="{{$invoiceData->seller->seller_co_information}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company-currency" class="col-sm-2 control-label">Currency</label>

                        <div class="col-sm-10">
                            <select class="form-control" id="company-currency" name="currency">
                                <option  value="N" {{ $invoiceData->inv_currency == 'N' ? 'selected':'' }}>NGN</option>
                                <option value="GBP" {{ $invoiceData->inv_currency == 'GBP' ? 'selected':'' }} >GBP</option>
                                <option value="$" {{ $invoiceData->inv_currency == '$' ? 'selected':'' }}>USD</option>
                                <option value="$" {{ $invoiceData->inv_currency == '$' ? 'selected':'' }}>CAD</option>
                                <option value="Y" {{ $invoiceData->inv_currency == 'Y' ? 'selected':'' }}>YEN</option>
                                <option value="Z" {{ $invoiceData->inv_currency == 'Z' ? 'selected':'' }}>ZAR</option>
                            </select>
                        </div>
                    </div>
                </div>

                <legend>Configuration</legend>
                <div class="form-group">
                    <label for="invoice-type-radio" class="col-sm-2 control-label">Invoice Type</label>

                    <div class="col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="radio" name="invoiceType" value="Tax" {{ $invoiceData->inv_type == 'Tax' ? 'checked' : ''}}> Tax invoice
                            </label>
                            <label>
                                <input type="radio" id="invoice-type-radio" name="invoiceType" value="Retail" {{ $invoiceData->inv_type == 'Retail' ? 'checked' : ''}}>
                                Retail invoice
                            </label>
                        </div>
                    </div>
                </div>

                <legend>Purchaser Info.</legend>
                <div class="form-group">
                    <label for="billNo" class="col-sm-2 control-label">Date</label>

                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="purDate" value="{{$invoiceData->purchaser->pur_date}}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="purchaser" class="col-sm-2 control-label" >Name</label>

                    <div class="col-xs-4">
                        <input type="text" class="form-control" value="{{$invoiceData->purchaser->pur_name}}" required name="purName">
                    </div>
                </div>

                <div class="form-group">
                    <label for="purchaser-address" class="col-sm-2 control-label">Address</label>

                    <div class="col-xs-4">
                    <textarea name="purAddress" rows="3" class="form-control">{{$invoiceData->purchaser->pur_address}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="purchaser-tin" class="col-sm-2 control-label">Tax Id No.</label>

                    <div class="col-xs-4">
                        <input type="text" class="form-control" name="purTaxId" value="{{$invoiceData->purchaser->pur_tax_id}}">
                    </div>
                </div>

                <legend>Items</legend>
                <div class="row heading">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-5">-Name</div>
                    <div class="col-sm-2">-Price</div>
                    <div class="col-sm-2">-Unit/Quantity</div>
                </div>
                <div class="items">
                    @php $i=1;$ItemTotal=0;@endphp
                    @foreach($invoiceData->item as $item)
                    <div class="row item">
                        <div class="col-sm-1">{{$i++}}</div>
                        <div class="col-sm-5">
                            <input class="form-control" name="itemId[]" value="{{$item->id}}" type="hidden">
                            <input class="form-control" name="itemName[]" value="{{$item->inv_item_name}}" type="text">
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon">N</span>
                                <input class="form-control" name="itemPrice[]" value="{{$item->inv_item_price}}" type="number">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" name="itemQty[]"  value="{{$item->inv_item_quantity}}"  type="number">
                        </div>
                        <div class="col-sm-2">
                            <p class="form-control-static"><span class="currency">{{$invoiceData->inv_currency}}</span>
                                <span class="numeral">{{$I_Total=$item->inv_item_price*$item->inv_item_quantity}}</span></p>
                            @php $ItemTotal+=$I_Total @endphp
                        </div>
                    </div>

                    @endforeach
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Total</label>

                    <div class="col-sm-offset-8 col-sm-2">
                        <p class="form-control-static"><span class="currency">{{$invoiceData->inv_currency}}</span> <span
                                    class="numeral">{{$ItemTotal}}</span></p>
                    </div>
                </div>

                <legend>Discount</legend>
                <div class="form-group">
                    <label for="discount" class="col-sm-2 control-label">Percentage</label>

                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="number" class="form-control" name="disRate" id="discount" value="{{$invoiceData->inv_dis_per}}" placeholder="Discount"
                                   max="100" min="0">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="col-sm-offset-6 col-sm-2">
                        <p class="form-control-static discount-amount">- <span class="currency">{{$invoiceData->inv_currency}}</span> <span
                                    class="numeral">{{$discount=($ItemTotal/100)*$invoiceData->inv_dis_per}}</span></p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Total</label>

                    <div class="col-sm-offset-8 col-sm-2">
                        <p class="form-control-static discount-total"><span class="currency">{{$invoiceData->inv_currency}}</span> <span
                                    class="numeral">{{$discountTotal=$ItemTotal-$discount}}</span></p>
                    </div>
                </div>

                <legend>Taxes</legend>
                <div class="form-group">
                    <label for="tax-type-check" class="col-sm-2 control-label" >Tax Type</label>

                    <div class="col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="radio" name="taxType" value="VAT" {{ $invoiceData->inv_tax_type == 'VAT' ? 'checked' : ''}}> VAT
                            </label>
                            <label>
                                <input type="radio" id="tax-type-check" name="taxType" value="CST" {{ $invoiceData->inv_tax_type == 'CST' ? 'checked' : ''}}> CST
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="vat" class="col-sm-2 control-label">Percentage</label>

                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="number" class="form-control" id="vat" name="taxRate" value="{{$invoiceData->inv_tax_per}}" step="0.1">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="col-sm-offset-6 col-sm-2">
                        <p class="form-control-static vat-amount"><span class="currency">{{$invoiceData->inv_currency}}</span> <span
                                    class="numeral">{{$tax=($discountTotal/100)*$invoiceData->inv_tax_per}}</span></p>
                    </div>
                </div>

                <div class="service-tax-group hide">
                    <div class="form-group">
                        <label for="service-tax" class="col-sm-2 control-label">Service Tax</label>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="number" class="form-control" id="service-tax" value="0"
                                       placeholder="Service Tax" step="0.1">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                        <div class="col-sm-offset-6 col-sm-2">
                            <p class="form-control-static service-tax-amount"><span class="currency">N</span> <span
                                        class="numeral">0</span></p>
                        </div>
                    </div>

                    <div class="service-tax-extras">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Swachchh Bharat Cess (0.5%)</label>

                            <div class="col-sm-offset-6 col-sm-2">
                                <p class="form-control-static bharat-cess"><span class="currency">N</span> <span
                                            class="numeral">0</span></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Krishi Kalyan Cess (0.5%)</label>

                            <div class="col-sm-offset-6 col-sm-2">
                                <p class="form-control-static kk-cess"><span class="currency">N</span>
                                    <span class="numeral">0</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                @if(auth()->check())
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Invoice Logo</label>

                        <div class="col-sm-offset-8 col-sm-2">
                            <input type="file" name="logo" value="{{$invoiceData->inv_logo}}" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Signature
                        </label>
                         
                        <div class="col-sm-offset-6 col-sm-4">
                		<div id="signArea" >
                			<div class="sig sigWrapper" style="height:auto;">
                				<div class="typed"></div>
                			    	<canvas class="sign-pad" id="sign-pad" width="350" height="100"></canvas>
                                 <input type="hidden" value="" id="imgurl" name="img_data" />
                            </div>
                		</div>
                        </div>
                    </div>                      
            @endif
            <!--<div class="form-group">-->
                <!--<label class="col-sm-2 control-label">Total</label>-->
                <!--<div class="col-sm-offset-8 col-sm-2">-->
                <!--<p class="form-control-static total-with-tax">&#8377; 0</p>-->
                <!--</div>-->
                <!--</div>-->

                <hr/>
                <!--<div class="form-group">-->
                <!--<label class="col-sm-2 control-label">Round off</label>-->
                <!--<div class="col-sm-offset-8 col-sm-2">-->
                <!--<p class="form-control-static round-off">+ &#8377; 0.00</p>-->
                <!--</div>-->
                <!--</div>-->

                <div class="form-group">
                    <label class="col-sm-2 control-label">Total payable</label>

                    <div class="col-sm-offset-8 col-sm-2">
                        <p class="form-control-static total-with-tax"><span class="currency">{{$invoiceData->inv_currency}}</span> <span
                                    class="numeral">{{$tax+$discountTotal}}</span></p>
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-sm-10">
                        <div class="btn-group-lg">
                            <button class="btn btn-primary new-bill" type="submit">Update</button>
                            {{--@if(auth()->check())--}}
                                {{--<button type="submit" class="btn btn-warning" role="button">Save Invoice Online</button>--}}
                            {{--@else--}}
                                {{--<a href="{{route('pricing')}}" target="_blank" class="btn btn-warning" role="button">Save Invoice Online</a>--}}
                        {{--@endif--}}

                        <!-- <a href="https://shoppersbill.com/logines/invoices.php" class="btn btn-mybutton" role="button">Search</a>
                    <a href="https://shoppersbill.com/logins/login.php" class="btn btn-mybutton" role="button">Login</a>
                    <a href="https://shoppersbill.com/invos" class="btn btn-mybutton" role="button">Send Invoice</a>-->
                        </div>
                    </div>
                </div>
        </div>
    </form>

    <div class="row" style="padding: 5% 0px;margin:0px;">

    </div>
    </div>
@endsection
@section('script')
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="{!! asset('js/numeric-1.2.6.min.js') !!}"></script> 
<script src="{!! asset('js/bezier.js') !!}"></script>
<script src="{!! asset('js/jquery.signaturepad.js') !!}"></script> 
	
<script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
<script src="{!! asset('js/json2.min.js') !!}"></script>  
<script>
			$(document).ready(function() {
				$('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
			}); 
			$("#form").on('submit',function(e){
			    e.preventDefault();
				html2canvas([document.getElementById('sign-pad')], {
					onrendered: function (canvas) {
						var canvas_img_data = canvas.toDataURL('image/png');
						var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
						$('#imgurl').val(img_data);
                        $('#form').unbind('submit').submit();
					}
				});
			});			
</script>    
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" crossorigin="anonymous"></script>
    {{--<script src="{{asset('js/pdfmake.js')}}"></script>--}}
    {{--<script src="{{asset('js/vfs_fonts.js')}}"></script>--}}
    {{--<script src="{{asset('js/enums.js')}}"></script>--}}
    {{--<script src="{{asset('js/utils.js')}}"></script>--}}
    {{--<script src="{{asset('js/text_number.js')}}"></script>--}}
    {{--<script src="{{asset('js/bill_writer.js')}}"></script>--}}
    {{--<script src="{{asset('js/invoiceMain.js')}}"></script>--}}


@endsection