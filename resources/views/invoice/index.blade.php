@extends('layouts.master')
@section('headerbox')
    <!--PRICING AREA-->
    <section style="background-image: linear-gradient(to bottom, #1193d4 , transparent,transparent,transparent,transparent);" class="price-area padding-100-70 sky-gray-bg" id="pricing">
        <div class="container">
            <br>
            <br>
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <a href="{{route('account')}}" class="list-group-item">Dashboard</a>
                        <a href="{{route('invoice.index')}}" class="list-group-item active">Invoices</a>
                        <a href="#" class="list-group-item">Settings</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="table-responsive">
                        <form action="{{route('search')}}" class="search-form" method="POST" role="search" style="border: 2px solid #1193d4;">
                            @csrf
                            <input type="search" name="search" id="search" placeholder="Invoice Serial Number">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>Invoice No.</th>
                                <th>Purchaser Name</th>
                                <th>Purchaser Email</th>
                                <th>Items</th>
                                <th>Invoice Total</th>
                                <th>Created</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                        @php $i=1; $itemsTotal=0;$taxTotal=0; $discountTotal=0; @endphp
                           @foreach($invoiceData as $inv)

                            <tr>
                                <td>{{$inv->id}}</td>
                                <td>{{$inv->inv_number}}</td>
                                <td>{{$inv->purchaser->pur_name}}</td>
                                <td>purchaser mail</td>
                                <td>
                                    <ul style="font-size: 10px; font-weight:bolder;">
                                    @foreach($inv->item as $item)
                                      <li>{{$item->inv_item_name}}</li>
                                         @php $itemsTotal=$itemsTotal+$item->inv_item_price*$item->inv_item_quantity @endphp
                                    @endforeach
                                    </ul>
                                </td>
                                @php $taxTotal=$itemsTotal+(($inv->inv_tax_per/100)*$itemsTotal)@endphp
                                <td>{{$discountTotal=$taxTotal-(($inv->inv_dis_per/100)*$taxTotal)}} {{$inv->inv_currency}}</td>
                                <td>{{$inv->created_at->diffForHumans()}}</td>
                                <td><a href="{{route('invoice.edit',$inv->id)}}"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;<a href="{{route('invoice.show',$inv->id)}}"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;<a href="{{route('pdf',$inv->id)}}"><i class="fa fa-download"></i></a></td>

                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <span>{{$invoiceData->onEachSide(1)->links()}}</span>
                    </div>
                </div>



            </div>
        </div>
    </section>
    <!--PRICING AREA END-->
@endsection

@section('script')
    <script type="text/javascript">
        $('#search').on('keyup',function(){
            $value=$(this).val();
            $.ajax({
                type : 'get',
                url : '{{route('search')}}',
                data:{'search':$value},
                success:function(data){
                    $('tbody').html(data);
                }
            });
        })
    </script>
    <script type="text/javascript">
        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    </script>
@endsection()