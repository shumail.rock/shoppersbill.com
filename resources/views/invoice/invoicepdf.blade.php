<table width="100%" style="margin:auto;">
    <tr>
        <td>
            <table width="100%" style="border-bottom:2px solid black;padding-bottom: 5px;">
                <tr>
                    <td align="right">{{$invoice->seller->seller_name}}</td>
                </tr>
                <tr>
                    <th align="center"><h1>Retail Invoice/Bil</h1></th>
                </tr>
                <tr>
                    <th align="center"><img src="{{asset('inovieLogos')}}/{{$invoice->inv_logo}}" style="width: 150px;height: 100px;object-fit:contain;"></th>
                </tr>
                <br/>
                <tr>
                    <td align="center">{{$invoice->seller->seller_address}}</td>
                </tr>
                <tr>
                    <td align="center">TIN:{{$invoice->seller->seller_tax_id}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" style="border-bottom:2px solid black;margin-bottom: 10px;padding-bottom: 10px;">
                <tr>
                    <th>Invoice: {{$invoice->inv_number}}</th>
                    <th>{{$invoice->purchaser->pur_name}}</th>
                </tr>
                <tr>
                    <td>Date: {{date('d-m-Y')}}</td>
                    <td>{{$invoice->purchaser->pur_address}}</td>
                </tr>
                <tr>
                    <td></td><td>TIN/PAN:{{$invoice->purchaser->pur_tax_id}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table width="100%" style="margin-bottom: 50px;">
                <tr>
                    <th style="border-bottom: 2px solid black;" align="center">Sr</th><th style="border-bottom: 2px solid black;" align="center">Name</th><th style="border-bottom: 2px solid black;" align="center">Price</th><th style="border-bottom: 2px solid black;" align="center">Quantity</th><th style="border-bottom: 2px solid black;" align="center">Amount</th>
                </tr>
                @php $i=1; $itemsTotal=0;$taxTotal=0; $discountTotal=0; @endphp
                @foreach($invoice->item as $item)
                <tr>
                    <td align="center" style="border-bottom: 1px solid #ddd;">{{$i++}}</td>
                    <td align="center" style="border-bottom: 1px solid #ddd;">{{$item->inv_item_name}}</td>
                    <td align="center" style="border-bottom: 1px solid #ddd;">{{$item->inv_item_price}}</td>
                    <td align="center" style="border-bottom: 1px solid #ddd;">{{$item->inv_item_quantity}}</td>
                    <td align="center" style="border-bottom: 1px solid #ddd;">{{$item->inv_item_price* $item->inv_item_quantity}}</td>
                    @php $itemsTotal=$itemsTotal+($item->inv_item_price* $item->inv_item_quantity) @endphp
                </tr>
                @endforeach
                <tr>
                    <td colspan="3" style="border-bottom: 1px solid #ddd;"></td><th style="border-bottom: 1px solid #ddd;" align="center">Total</th><th style="border-bottom: 1px solid #ddd;" align="center">{{$itemsTotal}}</th>
                </tr>
                <tr>
                    <td colspan="3" style="border-bottom: 1px solid #ddd;"></td><td style="border-bottom: 1px solid #ddd;" align="center">Discount({{$invoice->inv_dis_per}}%)</td><td style="border-bottom: 1px solid #ddd;" align="center">{{$discount=($itemsTotal/100)*$invoice->inv_dis_per}}</td>
                </tr>
                <tr>
                    <td colspan="3" style="border-bottom: 1px solid #ddd;"></td><th style="border-bottom: 1px solid #ddd;" align="center">Total</th> <th style="border-bottom: 1px solid #ddd;" align="center">{{$discountTotal=$itemsTotal-$discount}}</th>
                </tr>
                <tr>
                    <td colspan="3" style="border-bottom: 1px solid #ddd;"></td><td style="border-bottom: 1px solid #ddd;" align="center">{{$invoice->inv_tax_type}}({{$invoice->inv_tax_per}}%)</td><td style="border-bottom: 1px solid #ddd;" align="center">{{$tax=($discountTotal/100)*$invoice->inv_tax_per}}</td>
                </tr>
                <tr>
                    <td colspan="3" style="border-bottom: 1px solid #ddd;"></td><th style="border-bottom: 1px solid #ddd;" align="center">Total Payable amount</th><th style="border-bottom: 1px solid #ddd;" align="center">{{$tax+$discountTotal}}</th>
                </tr>

            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td>
                        <h3>Terms & Conditions</h3>
                        <ol style="font-size: 10px;">

                            <li>Goods once sold cannot be taken back.</li>
                            <li>Interest @18% pa chargeable on bills unpaid for more than 15 days.</li>
                            <li>Dispute will be under Delhi jurisdiction.</li>
                            <li>This is a computer generated invoice and is not valid without authority signatures.</li>
                            <li>E & OE</li>

                        </ol>
                    </td>
                    <th style="text-align:right;"><img src="{{asset('inovieLogos')}}/{{$invoice->inv_signature}}" style="height:100px; width:200px; object-fit:contain;" /></th>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td align="right"><span style="color: red">For {{$invoice->seller->seller_name}}</span></td>
                </tr>
            </table>
        </td>
    </tr>

</table>