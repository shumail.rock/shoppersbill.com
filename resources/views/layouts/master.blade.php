<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <!--====== USEFULL META ======-->

    <meta name="_token" content="{{ csrf_token() }}">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="ShoppersBill" />
    <meta name="keywords" content="Online Invoicing and Reciept Platform" />

    <!--====== TITLE TAG ======-->
    <title>ShoppersBills</title>

    <!--====== FAVICON ICON =======-->
    <link rel="shortcut icon" type="image/ico" href="" />

    <!--====== STYLESHEETS ======-->
    <link rel="stylesheet" href="{!! asset('css/normalize.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/animate.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/modal-video.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/stellarnav.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/owl.carousel.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/slick.css') !!}">
    <link href="{!! asset('css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/font-awesome.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/material-icons.css') !!}" rel="stylesheet">
    
    <link href="{{asset('css/jquery.signaturepad.css')}}" rel="stylesheet">
    
    <!--====== MAIN STYLESHEETS ======-->
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/responsive.css') !!}" rel="stylesheet">
    @yield('style')
    <script src="{!! asset('js/vendor/modernizr-2.8.3.min.js') !!}"></script>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<style>    
@media only screen and (max-width: 600px) {
 #signup {
            width: 100%;
            margin: 60px auto;
            padding:10px;
            background: white;
            text-align: center;
            -webkit-box-shadow: 2px 2px 3px rgba(0,0,0,0.1);
            box-shadow: 2px 2px 3px rgba(0,0,0,0.1);
        }
}
</style>
</head>

<body class="home-one" data-spy="scroll" data-target=".mainmenu-area" data-offset="90">




<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!--- PRELOADER -->
<div class="preeloader">
    <div class="preloader-spinner"></div>
</div>

<!--SCROLL TO TOP-->
<a href="#home" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

<!--START TOP AREA-->
<header class="{{Request::route()->getName()=='/' ? "top-area" : ""}} {{Request::route()->getName()=='about-us' ? "top-area" : ""}}" id="home">
    <div class="header-top-area">
        <!--MAINMENU AREA-->
        <div class="mainmenu-area" id="mainmenu-area">
            <div class="mainmenu-area-bg"></div>
            <nav class="navbar">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a href="{{route('/')}}" class="navbar-brand"><img src="{{ asset('img/logo.png') }}" alt="logo"></a>
                    </div>
                    <div id="main-nav" class="stellarnav">
                        <div class="search-and-signup-button white pull-right hidden-md hidden-sm hidden-xs">
                            <button data-toggle="collapse" data-target="#search-form-switcher"><i class="fa fa-search"></i></button>
                            @if(!auth()->check())
                            <a href="{{route('signup')}}" class="sign-up">SignUp</a>
                            @endif
                        </div>
                        <ul id="nav" class="nav">
                            <li class="active"><a href="{{route('/')}}">home</a>
                            </li>
                            <li><a href="{{route('invoice.create')}}">Create Invoice</a></li>
                            <li><a href="#">Search</a></li>
                            <li><a href="{{route('pricing')}}">Pricing</a></li>
                            <li><a href="{{route('about-us')}}">About us</a></li>
                            @if(!auth()->check())
                            <li><a href="{{route('login')}}">Login</a></li>
                            @else
                            <li><a href="{{route('account')}}">My Account</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            <div id="search-form-switcher" class="search-collapse-area collapse white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="white">
                                <form action="#" class="search-form">
                                    <input type="search" name="search" id="search" placeholder="Search Here..">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--END MAINMENU AREA END-->
    </div>
    @yield('headerbox')

</header>
<!--END TOP AREA-->



@yield ('content')

<!--FOOER AREA-->
<div class="footer-area white">
    <div class="footer-top-area blue-bg padding-100-50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="subscriber-form-area" style="visibility: visible;">
                        <div class="footer-bottom-area blue-bg">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="footer-social-bookmark text-center wow fadeIn">
                                            <div class="footer-logo mb50">
                                                <a href="#"><img src="img/logo.png" alt=""></a>
                                            </div>
                                            <ul class="social-bookmark" style="visibility: visible;">
                                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                                                <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="footer-copyright text-center wow fadeIn">
                                            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designs with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://Shoppersbill.com" target="_blank">ShoppersBill</a> <a href="http://www.shoppersbill.com/terms.html"> --Terms and Condition-- </a><a href="http://www.shoppersbill.com/policy.html"> --Privacy Policy-- </a> </a><a href="http://www.shoppersbill.com/contactus.html"> --Contact us-- </a>
                                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--FOOER AREA END-->


                    <!--====== SCRIPTS JS ======-->
                    <script src="{!! asset('js/vendor/jquery-1.12.4.min.js') !!}"></script>
                    <script src="{!! asset('js/vendor/bootstrap.min.js') !!}"></script>

                    <!--====== PLUGINS JS ======-->
                    <script src="{!! asset('js/vendor/jquery.easing.1.3.js') !!}"></script>
                    <script src="{!! asset('js/vendor/jquery-migrate-1.2.1.min.js') !!}"></script>
                    <script src="{!! asset('js/vendor/jquery.appear.js') !!}"></script>
                    <script src="{!! asset('js/owl.carousel.min.js') !!}"></script>
                    <script src="{!! asset('js/slick.min.js') !!}"></script>
                    <script src="{!! asset('js/stellar.js') !!}"></script>
                    <script src="{!! asset('js/wow.min.js') !!}"></script>
                    <script src="{!! asset('js/jquery-modal-video.min.js') !!}"></script>
                    <script src="{!! asset('js/stellarnav.min.js') !!}"></script>
                    <script src="{!! asset('js/contact-form.js') !!}"></script>
                    <script src="{!! asset('js/jquery.ajaxchimp.js') !!}"></script>
                    <script src="{!! asset('js/jquery.sticky.js') !!}"></script>	
                    

                    <!--===== ACTIVE JS=====-->
                    <script src="{!! asset('js/main.js') !!}"></script>

                    <!--Start of Tawk.to Script-->
                    <script type="text/javascript">
                        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                        (function(){
                            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                            s1.async=true;
                            s1.src='https://embed.tawk.to/5bf0ad8b40105007f3787bd5/default';
                            s1.charset='UTF-8';
                            s1.setAttribute('crossorigin','*');
                            s0.parentNode.insertBefore(s1,s0);
                        })();
                    </script>
                    <!--End of Tawk.to Script-->
                    @yield('script')

</body>

</html>