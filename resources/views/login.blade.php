@extends('layouts.master')
@section('style')
    <style>
        #signup {
            width: 60%;
            margin: 60px auto;
            background: white;
            padding: 60px 120px 80px 120px;
            text-align: center;
            -webkit-box-shadow: 2px 2px 3px rgba(0,0,0,0.1);
            box-shadow: 2px 2px 3px rgba(0,0,0,0.1);
        }
       
        label {
            display: block;
            position: relative;
            margin: 40px 0px;
        }
        .input {
            width: 100%;
            padding: 10px;
            background: transparent;
            border: none;
            outline: none;
        }

        .line-box {
            position: relative;
            width: 100%;
            height: 2px;
            background: #BCBCBC;
        }

        .line {
            position: absolute;
            width: 0%;
            height: 2px;
            top: 0px;
            left: 50%;
            transform: translateX(-50%);
            background: #1193d4;
            transition: ease .6s;
        }

        .input:focus + .line-box .line {
            width: 100%; background: transparent !important;
        }
        .input:-webkit-autofill {
            -webkit-box-shadow: 0 0 0 100px transparent inset !important;
            background: transparent !important;
        }
        .label-txt {
            position: absolute;
            top: -1.6em;
            padding: 10px;
            font-family: sans-serif;
            font-size: .8em;
            letter-spacing: 1px;
            color: rgb(120,120,120);
            transition: ease .3s;
        }

        .label-active {
            top: -3em;
        }

        button {
            display: inline-block;
            padding: 12px 24px;
            background: rgb(220,220,220);
            font-weight: bold;
            color: rgb(120,120,120);
            border: none;
            outline: none;
            border-radius: 3px;
            cursor: pointer;
            transition: ease .3s;
        }

        button:hover {
            background: #1193d4;
            color: #ffffff;
        }
    </style>
@endsection
@section('content')
    <section style="background-image: linear-gradient(to bottom, #1193d4 , transparent,transparent,transparent,transparent);" class="price-area padding-100-70 sky-gray-bg" id="pricing">
        <br>
        <br>
        <br>
        <br>
        <form id="signup" method="post" action="{{ route('login') }}">
            @csrf
            <h1 style="text-align:center; ">Login</h1>
            <label>
                <p class="label-txt">ENTER YOUR EMAIL</p>
                <input type="email" name="email" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>

            <label>
                <p class="label-txt">ENTER YOUR PASSWORD</p>
                <input type="password" name="password" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
            <div class="form-group row">
                <div class="col-md-6 offset-md-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>
            <button type="submit">Login</button>
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
        </form>
    </section>
@endsection