@extends('layouts.master')
@section('headerbox')
<!--PRICING AREA-->
<section style="background-image: linear-gradient(to bottom, #1193d4 , transparent,transparent,transparent,transparent);" class="price-area padding-100-70 sky-gray-bg" id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    <h2>Pricing <span>Table</span></h2>
                    <span class="icon-and-border"><i class="material-icons">phone_android</i></span>
                    <p>Check below to see our Pricing Offer.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-price center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="price-hidding">
                        <h2>ShoppersBill - Free Usage</h2>
                    </div>
                    <div class="price-rate">
                        <h2><sup>$</sup>0.00
                            <sub>Unlimited & Free</sub>
                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>Create Unlimited Invoice, Receipt & Bills</li>
                            <li> Unlimited Upload</li>
                            <li>No Tech support</li>
                        </ul>
                    </div>
                    <div class="buy-now-button">
                        <a href="#" class="read-more">Create for free</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-price center wow fadeInUp" data-wow-delay="0.3s">
                    <div class="price-hidding">
                        <h2>ShoppersBill - One-off (1-Hour)</h2>
                    </div>
                    <div class="price-rate">
                        <h2><sup>$</sup>0.45
                            <sub>1 Hour + 15% off</sub>
                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>Create Unlimited Invoice, Receipt & Bills</li>
                            <li>1 Hour Unlimited Upload +Backup</li>
                            <li>1/Month Tech support</li>
                            <li>Save Invoice & Receipt Online</li>
                        </ul>
                    </div>
                    <div class="buy-now-button">
                        <a href="#" class="read-more">Purchase</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-price center wow fadeInUp" data-wow-delay="0.4s">
                    <div class="price-hidding">
                        <h2>ShoppersBill - One Month Package (Monthly)</h2>
                    </div>
                    <div class="price-rate">
                        <h2><sup>$</sup>2.80
                            <sub>1 Month </sub>
                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>Create Unlimited Invoice, Receipt & Bills</li>
                            <li>1/Month Unlimited Upload + Backup</li>
                            <li>1/Month Tech support</li>
                            <li> Save Invoices & Receipt Online </li>
                        </ul>
                    </div>
                    <div class="buy-now-button">
                        <a href="#" class="read-more">Purchase</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-price center wow fadeInUp visible-sm" data-wow-delay="0.5s">
                    <div class="price-hidding">
                        <h2>ShoppersBill - Yearly Package</h2>
                    </div>
                    <div class="price-rate">
                        <h2><sup>$</sup>28
                            <sub>1 Year</sub>
                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>Create Unlimited Invoice, Receipt & Bills</li>
                            <li>1 Year Unlimited Upload + Backup</li>
                            <li>24x7 Tech support</li>
                        </ul>
                    </div>
                    <div class="buy-now-button">
                        <a href="#" class="read-more">Purchase</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--PRICING AREA END-->
@endsection