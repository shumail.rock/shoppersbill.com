@extends('layouts.master')
@section('style')
<style>


    #signup {
    width: 60%;
    margin: 60px auto;
    background: white;
    padding: 60px 120px 80px 120px;
    text-align: center;
    -webkit-box-shadow: 2px 2px 3px rgba(0,0,0,0.1);
    box-shadow: 2px 2px 3px rgba(0,0,0,0.1);
    }
    label {
    display: block;
    position: relative;
    margin: 40px 0px;
    }
    .input {
    width: 100%;
    padding: 10px;
    background: transparent;
    border: none;
    outline: none;
    }

    .line-box {
    position: relative;
    width: 100%;
    height: 2px;
    background: #BCBCBC;
    }

    .line {
    position: absolute;
    width: 0%;
    height: 2px;
    top: 0px;
    left: 50%;
    transform: translateX(-50%);
    background: #1193d4;
    transition: ease .6s;
    }

    .input:focus + .line-box .line {
    width: 100%; background: transparent !important;
    }
    .input:-webkit-autofill {
        -webkit-box-shadow: 0 0 0 100px transparent inset !important;
        background: transparent !important;
    }
    .label-txt {
    position: absolute;
    top: -1.6em;
    padding: 10px;
    font-family: sans-serif;
    font-size: .8em;
    letter-spacing: 1px;
    color: rgb(120,120,120);
    transition: ease .3s;
    }

    .label-active {
    top: -3em;
    }

    button {
    display: inline-block;
    padding: 12px 24px;
    background: rgb(220,220,220);
    font-weight: bold;
    color: rgb(120,120,120);
    border: none;
    outline: none;
    border-radius: 3px;
    cursor: pointer;
    transition: ease .3s;
    }

    button:hover {
    background: #1193d4;
    color: #ffffff;
    }
    .error{
        color:red;
    }
 </style>
@endsection
@section('content')
    <section style="background-image: linear-gradient(to bottom, #1193d4 , transparent,transparent,transparent,transparent);" class="price-area padding-100-70 sky-gray-bg" id="pricing">
        <br>
        <br>
        <br>
        <br>
        <form id="signup" method="post" action="{{route('user.store')}}">
            @csrf
            <h1 style="text-align:center; ">SignUp</h1>
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('error')}}
                </div>
            @endif
            @if($errors->has('email'))
                <p class="error">{{$errors->first('email')}}</p>
            @endif
            <label>
                <p class="label-txt">ENTER YOUR EMAIL</p>
                <input type="email" name="email" value="{{old('email')}}" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
            @if($errors->has('firstname'))
                <p class="error">{{$errors->first('firstname')}}</p>
            @endif
            <label>
                <p class="label-txt">ENTER YOUR FIRST NAME</p>
                <input type="text" name="firstname" value="{{old('firstname')}}" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
            @if($errors->has('lastname'))
                <p class="error">{{$errors->first('lastname')}}</p>
            @endif
            <label>
                <p class="label-txt">ENTER YOUR LAST NAME</p>
                <input type="text" name="lastname" value="{{old('lastname')}}" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
            @if($errors->has('password'))
                <p class="error">{{$errors->first('password')}}</p>
            @endif
            <label>
                <p class="label-txt">ENTER YOUR PASSWORD</p>
                <input type="password" name="password" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
            <label>
                <p class="label-txt">ENTER YOUR PASSWORD AGAIN</p>
                <input type="password" name="password_confirmation" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
            @if($errors->has('membershipType'))
                <p class="error">{{$errors->first('membershipType')}}</p>
            @endif
            <label style="text-align: left;">
                <p class="label-txt">SELECT MEMBERSHIP TYPE </p><br>
                &nbsp;&nbsp;&nbsp;<input type="radio" value="1" {{old('membershipType')== '1' ? 'checked':''}} name="membershipType">&nbsp;<b>WEEKLY</b>
                &nbsp;&nbsp;&nbsp;<input type="radio" value="2" {{old('membershipType')== '2' ? 'checked':''}} name="membershipType">&nbsp;<b>MONTHLY</b>
                &nbsp;&nbsp;&nbsp;<input type="radio" value="3" {{old('membershipType')== '3' ? 'checked':''}} name="membershipType">&nbsp;<b>YEARLY</b>
            </label>
            <button type="submit">Signup</button>
        </form>
    </section>

@endsection