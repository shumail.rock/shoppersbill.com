@extends('layouts.master')
@section('headerbox')
    <div class="welcome-text-area white" >
        <div class="area-bg"></div>
        <div class="welcome-area">
            <div class="container">
                <div class="row flex-v-center">
                    <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                        <div class="welcome-mockup center">
                            <img src="img/home/watch-mockup.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                        <div class="welcome-text">
                            <h1>Welcome To ShoppersBill Invoice Apps.</h1>
                            <p> Generate Invoice and Reciept for everyone Including - shop Owners, Business, Freelancers,, store owners,Ecommerce, Transactions, Deals e.t.c.

                                .</p>
                            <div class="home-button">
                                <a href="#">Create Receipt / Invoice</a>
                                <a href="#">Search for Invoice </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
<!--FEATURES TOP AREA-->
<section class="features-top-area padding-100-50" id="features">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    <h2>welcome to <span>ShoppersBill</span></h2>
                    <span class="icon-and-border"><i class="material-icons">phone_android</i></span>
                    <p>ShoppersBill is an Online Invoicing and Reciept generation Platform ,For shop Owners, Business, Freelancers, and store owners, Generate and save your Invoice from any in the world, </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="qs-box relative mb50 center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="qs-box-icon">
                        <img src="img/icon/icon-1.png" height="70" width="70" alt="happy" class="picture">
                    </div>
                    <h3>Upload & Save</h3>
                    <p>Upload your Documents Invoice Certificates and Reciept ,let us help you save and keep it, Your Documents can been seen from all over the world .</p>
                    <a href="/create" class="read-more"> Click to Upload</a>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="qs-box relative mb50 center  wow fadeInUp" data-wow-delay="0.3s">
                    <div class="qs-box-icon">
                        <img src="img/icon/11.png" height="90" width="80" alt="happy" class="picture">
                    </div>
                    <h3>Create Invoice</h3>
                    <p>Get to Create and save Beautifully designed Invoice and Receipt that can be generated and viewed from anywhere in the world.</p>
                    <a href="/create/" class="read-more">Click to Create</a>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                <div class="qs-box relative mb50 center  wow fadeInUp" data-wow-delay="0.4s">
                    <div class="qs-box-icon">
                        <img src="img/icon/44.png" height="90" width="80" alt="happy" class="picture">
                    </div>
                    <h3>Search for Invoice</h3>
                    <p>Search for Invoice, Receipt, fliers,  Certficates, documents and Papew-works generated on our Platfrom.</p>
                    <a href="#" class="read-more">Click to Search</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--FEATURES TOP AREA END-->

<!--APP AREA-->
<section class="app-area relative padding-100-50 sky-gray-bg" id="app">
    <div class="app-area-mockup-bg"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6 col-sm-12 col-xs-12">
                <div class="area-title wow fadeIn">
                    <h2>More on <span>ShoppersBill</span></h2>
                    <span class="icon-and-border"><i class="material-icons">phone_android</i></span>
                    <p>.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <div class="qs-box relative mb50 left  wow fadeInUp" data-wow-delay="0.2s">
                            <div class="qs-box-icon">
                                <img src="img/icon/1.png" height="50" width="40" alt="happy" class="picture">
                            </div>
                            <h3>Create Invoice</h3>
                            <p>Generate and save invoice created for Customers or partners for Transactionon done, either in person or online.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <div class="qs-box relative mb50 left  wow fadeInUp" data-wow-delay="0.3s">
                            <div class="qs-box-icon">
                                <img src="img/icon/icon-1.png" height="90" width="80" alt="happy" class="picture">
                            </div>
                            <h3>Upload & Save</h3>
                            <p>Upload your Documents, Invoice, Certificates, and Reciept and let us help you save and secure Your Documents, Generated Invoice, can been seen from all over the world ..</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <div class="qs-box relative mb50 left  wow fadeInUp" data-wow-delay="0.2s">
                            <div class="qs-box-icon">
                                <img src="img/icon/Icon3.png" height="90" width="80" alt="happy" class="picture">
                            </div>
                            <h3>Search All</h3>
                            <p>Search for all generated Invoice, Receipt, fliers, Certficates, documents and Papew-works generated on our Platfrom.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <div class="qs-box relative mb50 left  wow fadeInUp" data-wow-delay="0.3s">
                            <div class="qs-box-icon">
                                <img src="img/icon/4.png" height="90" width="80" alt="happy" class="picture">

                                <h3>Secure & Reliable </h3>
                                <p>Our Services are secure and Reliable , all Generated Invoice Receipt and Documents are 100% secured except tampered by the Uploader.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<!--APP AREA END-->

<!--VIDEO AREA-->
<section class="video-area relative section-padding" id="video">
    <div class="area-bg" data-stellarr-ratio="0.6"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-5 col-md-offset-7 col-lg-offset-7 col-sm-12 col-xs-12">
                <div class="video-area-content padding-50-50 white wow fadeInRight" data-wow-delay="0.2s">
                    <h2>Description With Video</h2>
                    <p> Watch the Video Below to learn how to use ShoppersBill and the entire platform , to always have a wonderful experience</p>
                    <p>Learn the scerete to successfully generating Invoice, Documents and Certficates.</p>
                    <div class="video-play-button mt50">
                        <button data-video-id="tNmYebmUpL4" class="video-area-popup"><i class="fa fa-play-circle"></i></button>
                        <h3>Play The Video</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--VIDEO AREA END-->


<!--PRICING AREA-->
<section class="price-area padding-100-70 sky-gray-bg" id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    <h2>Pricing <span>Table</span></h2>
                    <span class="icon-and-border"><i class="material-icons">phone_android</i></span>
                    <p>Check below to see our Pricing Offer.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-price center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="price-hidding">
                        <h2>ShoppersBill - Free Usage</h2>
                    </div>
                    <div class="price-rate">
                        <h2><sup>$</sup>0.00
                            <sub>Unlimited & Free</sub>
                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>Create Unlimited Invoice, Receipt & Bills</li>
                            <li> Unlimited Upload</li>
                            <li>No Tech support</li>
                        </ul>
                    </div>
                    <div class="buy-now-button">
                        <a href="#" class="read-more">Create for free</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-price center wow fadeInUp" data-wow-delay="0.3s">
                    <div class="price-hidding">
                        <h2>ShoppersBill - One-off (1-Hour)</h2>
                    </div>
                    <div class="price-rate">
                        <h2><sup>$</sup>0.45
                            <sub>1 Hour + 15% off</sub>
                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>Create Unlimited Invoice, Receipt & Bills</li>
                            <li>1 Hour Unlimited Upload +Backup</li>
                            <li>1/Month Tech support</li>
                            <li>Save Invoice & Receipt Online</li>
                        </ul>
                    </div>
                    <div class="buy-now-button">
                        <a href="#" class="read-more">Purchase</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-price center wow fadeInUp" data-wow-delay="0.4s">
                    <div class="price-hidding">
                        <h2>ShoppersBill - One Month Package (Monthly)</h2>
                    </div>
                    <div class="price-rate">
                        <h2><sup>$</sup>2.80
                            <sub>1 Month </sub>
                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>Create Unlimited Invoice, Receipt & Bills</li>
                            <li>1/Month Unlimited Upload + Backup</li>
                            <li>1/Month Tech support</li>
                            <li> Save Invoices & Receipt Online </li>
                        </ul>
                    </div>
                    <div class="buy-now-button">
                        <a href="#" class="read-more">Purchase</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-price center wow fadeInUp visible-sm" data-wow-delay="0.5s">
                    <div class="price-hidding">
                        <h2>ShoppersBill - Yearly Package</h2>
                    </div>
                    <div class="price-rate">
                        <h2><sup>$</sup>28
                            <sub>1 Year</sub>
                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>Create Unlimited Invoice, Receipt & Bills</li>
                            <li>1 Year Unlimited Upload + Backup</li>
                            <li>24x7 Tech support</li>
                        </ul>
                    </div>
                    <div class="buy-now-button">
                        <a href="#" class="read-more">Purchase</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--PRICING AREA END-->
@endsection

