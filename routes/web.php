<?php

use App\Invoice;
use App\Payment;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Front-End Routes */
Route::get('/', function () {
    return view('welcome');
})->name('/');


Route::get('/signup', function () {
    return view('signup');
})->name('signup')->middleware('guest');

Route::resource('/user', 'UsersController');


Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/pricing', function () {
    return view('pricing');
})->name('pricing');

Route::get('/account', function () {

    $invoices=Invoice::where('user_id',auth()->user()->id)->get()->all();
    $totalInvoices=count($invoices);
    $userPlanEndsOn=Payment::select('payment_end_date')->where('user_id',auth()->user()->id)->get()->all();
    $userPlanEndsOn=$userPlanEndsOn[0]->payment_end_date;
    return view('account',compact('totalInvoices','userPlanEndsOn'));
})->name('account');

Route::get('/about-us', function () {
    return view('about-us');
})->name('about-us');

Route::resource('/invoice','InvoiceController');

Route::get('pdf/{id}','InvoiceController@GeneratePdf')->name('pdf');

Route::post('search','InvoiceController@InvoiceSearch')->name('search');

Route::get('paystack-callback-url','UsersController@paystackCallBack');




/* Admin Routes */

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
